<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Serializer\ItemSerializedName;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  itemOperations={"get"},
 *  collectionOperations={"get"},
 * )
 */
class WebflowCollection extends AbstractWebflowEntity {

    /**
     * @ApiProperty(identifier=true)
     * @ItemSerializedName("_id")
     */
    protected $id;

    private $name;

    private $slug;

    private $singularName;

    private $lastUpdated;

    private $createdOn;

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    public function setSlug(string $name): self {
        $this->slug = $name;
        return $this;
    }

    public function getSingularName(): ?string {
        return $this->singularName;
    }

    protected function setSingularName(string $name): self {
        $this->singularName = $name;
        return $this;
    }

    public function getLastUpdated(): \DateTimeInterface {
        return $this->lastUpdated ?? new \DateTimeImmutable($this->lastUpdated);
    }

    protected function setLastUpdated(string $data): self {
        $this->lastUpdated = new \DateTimeImmutable($data);
        return $this;
    }

    public function getCreatedOn(): \DateTimeInterface {
        return $this->createdOn ?? new \DateTimeImmutable($this->createdOn);
    }

    protected function setCreatedOn(string $data): self {
        $this->createdOn = new \DateTimeImmutable($data);
        return $this;
    }

}
