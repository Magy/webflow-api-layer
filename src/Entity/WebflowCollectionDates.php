<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Http\WebflowApi\AbstractWebflowApiClient;
use App\Serializer\ItemSerializedName;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  itemOperations={"get"},
 *  collectionOperations={"get"}
 * )
 */
class WebflowCollectionDates extends AbstractWebflowCollectionItem {

    /**
     * @ApiProperty(identifier=true)
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("_id")
     */
    protected $id;

    /*
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("_cid")
     */
    protected static $cid = '5ed6105a05af1d7d085a9002';

    /*
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     */
    private $name;

    /*
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     */
    private $slug;

    public static function cid(): string {
        return self::$cid;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    protected function setSlug(string $slug): self {
        $this->slug = $slug;
        return $this;
    }

}
