<?php

namespace App\Entity;

use App\Serializer\ItemSerializedName;


abstract class AbstractWebflowCollectionItem extends AbstractWebflowEntity {

    /**
     * @ItemSerializedName("_archived")
     */
    private $archived = false;

    /**
     * @ItemSerializedName("_draft")
     */
    private $draft = true;

    protected function setArchived(bool $a): self {
        $this->archived = $a;
        return $this;
    }

    public function getArchived(): ?bool {
        return $this->archived;
    }

    protected function setDraft(bool $a): self {
        $this->draft = $a;
        return $this;
    }

    public function getDraft(): ?bool {
        return $this->draft;
    }

    public function getCid(): string {
        return static::$cid;
    }

    abstract public static function cid(): string;
}
