<?php

namespace App\Entity;

use App\Http\WebflowApi\AbstractWebflowApiClient;
use App\Serializer\ItemSerializer;
use Doctrine\Common\Annotations\Reader;

abstract class AbstractWebflowEntity {

    private $webflowApiClient;

    private $fieldTranslation = [];

    private $leftOvers = [];

    public function __construct(?string $s = null) {
        $s;
    }

    public static function fromClient(AbstractWebflowApiClient $webflowApiClient, Reader $reader) : self {
        return (new static())->setClient($webflowApiClient)->addData($reader);
    }

    private function setClient(AbstractWebflowApiClient $webflowApiClient): self {
        $this->webflowApiClient = $webflowApiClient;
        return $this;
    }

    public function getId(): ?string {
        return $this->id;
    }

    protected function setId(string $s): self {
        $this->id = $s;
        return $this;
    }

    protected function getWebflowApiClient(): AbstractWebflowApiClient {
        return $this->webflowApiClient;
    }

    protected function getFieldMapping(): array {
        return $this->fieldTranslation;
    }

    private function addData(Reader $reader): self {
        $itemSerializer = new ItemSerializer($reader, $this);
        foreach($this->webflowApiClient->data as $field => $data) {
            if ($data != null && $fn = $itemSerializer->setFn($field))
                $fn($data);
            else
                $this->leftOvers[$field] = $data;
        }

        return $this;
    }

    public function getLeftOvers(): array {
        return $this->leftOvers;
    }

    public function getFields(): array {
        return [];
    }

}
