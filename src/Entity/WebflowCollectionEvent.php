<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Serializer\ItemSerializedName;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ApiResource(
 *  itemOperations={"get"},
 *  collectionOperations={"get" = {
 *      "normalization_context" = {
 *            "groups" = {"event:collection:read"},
 *      },
 *  }, "post" = {
 *      "denormalization_context" = {
 *          "groups" = {"event:collection:write"},
 *      },
 *  }}
 * )
 */

class WebflowCollectionEvent extends AbstractWebflowCollectionItem {

    /**
     * @ApiProperty(identifier=true)
     * @Groups({
     *  "event:collection:read",
     * })
     * @ItemSerializedName("_id")
     */
    protected $id;

    /**
     * @Groups({
     *  "event:collection:read",
     * })
     * @ItemSerializedName("_cid")
     */
    protected static $cid = '5ebbafce7b593028a5f8f29a';

    /**
     * @Groups({
     *  "event:collection:write",
     * })
     * @ItemSerializedName("@ignore")
     * @Assert\NotNull
     */
    private $captcha;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write"
     * })
     * @ItemSerializedName("name")
     * @Assert\NotNull
     */
    private $title;
    /*
     * @Groups({
     *  "event:collection:read",
     * })
     */
    private $slug;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("kjop-billett-link")
     * @Assert\Url(
     *  message = "Url '{{ value }}' er ugyldig. Husk begynne med http://"
     * )
     * @Assert\NotNull
     */
    private $bookingUrl;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("link-til-nettside")
     * @Assert\Url(
     *  message = "Url '{{ value }}' er ugyldig. Husk begynne med http://"
     * )
     * @Assert\NotNull
     */
    private $websiteUrl;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("kapasitet-tekst")
     * @Assert\NotNull(
     *  message= "Vennligst oppgi kapasitet"
     * )
     */
    private $capacity;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("sted-2")
     * @Assert\NotNull(
     *  message= "Vennligst oppgi lokalisjon for eventet"
     * )
     */
    private $place;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("organisasjonsnr")
     * @Assert\Length(
     *  min = 9,
     *  max = 9,
     *  allowEmptyString = false,
     * )
     * @Assert\NotNull
     */
    private $organizationNumber;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("arrangor")
     * @Assert\NotNull(
     *  message= "Vennligst oppgi arrangør"
     * )
     */
    private $organizer;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("kontaktperson")
     * @Assert\NotNull(
     *  message= "Vennligst angi kontaktperson"
     * )
     */
    private $contactPerson;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("e-post")
     * @Assert\Email(
     *  message = "Epostadresse '{{ value }}' er ugyldig",
     * )
     * @Assert\NotNull
     */
    private $contactEmail;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("telefonnummer")
     * @Assert\NotNull(
     *  message= "Vennligst angi en kontakt-telefon"
     * )
     */
    private $contactPhone;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @Assert\NotNull
     */
    private $description;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @Assert\Url(
     *  message = "Url '{{ value }}' er ugyldig. Husk begynne med http://"
     * )
     * @Assert\NotNull
     */
    private $image;

    /**
     * @ItemSerializedName("bilde")
     */
    private $bilde;

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("opptrer")
     * @Assert\Count(
     *  min = 1,
     *  minMessage = "Eventet må opptre minst {{ limit }} ganger"
     * )
     * @Assert\NotNull
     */
    private $happensOn = [];

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("kategorier")
     * @Assert\Count(
     *  min = 1,
     *  minMessage = "Eventet må ha minst {{ limit }} kategori"
     * )
     * @Assert\NotNull
     */
    private $categories = [];

    /**
     * @Groups({
     *  "event:collection:read",
     *  "event:collection:write",
     * })
     * @ItemSerializedName("malgruppe-r")
     * @Assert\Count(
     *  min = 1,
     *  minMessage = "Eventet må ha minst {{ limit }} målgruppe"
     * )
     * @Assert\NotNull
     */
    private $audiences = [];

    private $featured = false;

    public static function cid(): string {
        return self::$cid;
    }

    public function setCaptcha(string $c): self {
        $this->captcha = $c;
        return $this;
    }

    public function getCaptcha(): ?string {
        return $this->captcha;
    }
   
    public function getTitle(): ?string {
        return $this->title;
    }

    public function setTitle(string $n): self {
        $this->title = $n;
        return $this;
    }

    public function getSlug(): ?string {
        return $this->slug;
    }

    protected function setSlug(string $s): self {
        $this->slug = $s;
        return $this;
    }

    public function getContactEmail(): ?string {
        return $this->contactEmail;
    }

    public function setContactEmail(string $e): self {
        $this->contactEmail = $e;
        return $this;
    }

    public function getContactPhone(): ?string {
        return $this->contactPhone;
    }

    public function setContactPhone(string $s): self {
        $this->contactPhone = $s;
        return $this;
    }

    public function getBookingUrl(): ?string {
        return $this->bookingUrl;
    }

    public function setBookingUrl($s): self {
        $this->bookingUrl = $s;
        return $this;
    }

    public function getWebsiteUrl(): ?string {
        return $this->websiteUrl;
    }

    public function setWebsiteUrl(string $s): self {
        $this->websiteUrl = $s;
        return $this;
    }

    public function getCapacity(): ?string {
        return $this->capacity;
    }

    public function setCapacity(string $i): self {
        $this->capacity= $i;
        return $this;
    }

    public function getPlace(): ?string {
        return $this->place;
    }

    public function setPlace(string $s): self {
        $this->place = $s;
        return $this;
    }

    public function getContactPerson(): ?string {
        return $this->contactPerson;
    }

    public function setContactPerson(string $s): self {
        $this->contactPerson = $s;
        return $this;
    }

    public function getOrganizationNumber(): ?string {
        return $this->organizationNumber;
    }

    public function setOrganizationNumber(string $s): self {
        $this->organizationNumber = $s;
        return $this;
    }

    public function getOrganizer(): ?string {
        return $this->organizer;
    }

    public function setOrganizer(string $s): self {
        $this->organizer = $s;
        return $this;
    }

    public function getDescription(): ?string {
        return $this->description;
    }

    public function setDescription(string $s): self {
        $this->description = sprintf('<p>%s</p>', preg_replace('/[\\n]+/', '</p><p>', strip_tags($s)));
        return $this;
    }

    public function getImage() {
        return $this->image;
    }

    public function setImage($a): self {
        $this->image = $a;

        if (is_string($this->image))
            $this->bilde;

        return $this;
    }

    public function getBilde() {
        return $this->image;
    }

    public function getCategories(): ?array {
        $this->categories = array_map(function ($category) {
            return $category;
        }, $this->categories); 
        return $this->categories;
    }

    public function setCategories(array $a): self {
        return $this->setKategorier($a);
    }

    protected function setKategorier(array $a): self {
        $this->categories = $a;
        return $this;
    }

    public function getAudiences(): ?array {
        $this->audiences = array_map(function ($audience) {
            return $audience;
        }, $this->audiences);

        return $this->audiences;
    }

    public function setAudiences(array $a): self {
        $this->audiences = $a;
        return $this;
    }

    public function getHappensOn(): ?array {
        return $this->happensOn;
    }

    public function setHappensOn(array $a): self {
        $this->happensOn = $a;
        return $this;
    }

    public function getFeatured(): ?bool {
        return $this->featured;
    }

    protected function setFeatured(bool $f): self {
        $this->featured = $f;
        return $this;
    }

    /**
     * @Assert\Callback
     */

    public function validate(ExecutionContextInterface $context, $payload) {
    
        $happens = $this->getHappensOn();

        $violation = false;

        foreach ($happens as $key => $value) {
            $duration = floatval($value['duration'] ?? 0.0);
            if ($duration < 0.25 || $duration > 24.0) {
                $context->buildViolation("Varighet må være angitt og mellom 0.25 og 24")->addViolation();
                $violation = true;
            }

            $date = new \DateTime($value['date'] ?? "2005-05-05 05:05:05");
            $fStart = new \DateTime('2020-06-25 00:00:01');
            $fEnd = new \DateTime('2020-08-09 23:59:00');

            if ($date < $fStart || $date > $fEnd) {
                $context->buildViolation("Eventet må skje mellom 25/6 - 09/8, år 2020.")->addViolation();
                $violation = true;
            }

            if ($violation)
                break;

        }

    }

}
