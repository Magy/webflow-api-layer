<?php

namespace App\Http\WebflowApi;

use App\Http\WebflowApiClient;
use App\Http\WebflowApiClientInterface;

abstract class AbstractWebflowApiCollection extends AbstractWebflowApiClient {

    private $data = null;
    protected $parent = null;

    public function __construct(WebflowApiClient $wpapi, ?AbstractWebflowApiClient $parent = null) {
        parent::__construct($wpapi);
        $this->parent = $parent;
    }

    abstract protected function getLoadScope(): string;

    protected function getData(): array {
        return $this->data ?? [];
    } 

    protected function getEntries(string $of): array {
        if ($this->data != null)
            return $this->data;

        $req = $this->getClient()->get($this->getLoadScope());

        if ($req->getStatusCode() != 200)
            return [];

        $this->data = [];

        foreach ($req->toArray() as $key => $entry) {
            if (!is_null($entry = $this->createEntry($key, $entry)))
                $this->addEntry($entry);
        }

        return $this->data;

    }

    public function addEntry(AbstractWebflowApiField $f): self {
        $this->data[] = $f;
        return $this;
    }

    abstract public function createEntry($key, $value): ?AbstractWebflowApiField;
}

?>
