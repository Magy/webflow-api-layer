<?php

namespace App\Http\WebflowApi;

class WebflowSite extends AbstractWebflowApiField {

    protected function getLoadScope(): string
    {
        return sprintf('sites/%s', $this->data['_id']);
    }

    public function getCollections(): ?WebflowApiCollections {
        return $this->loadCollection(
            sprintf('sites/%s/collections', $this->data['_id']),
            WebflowApiCollections::class,
        );
    }
}

?>
