<?php

namespace App\Http\WebflowApi;

class WebflowSites extends AbstractWebflowApiCollection {

    public function getLoadScope(): string {
        return '/sites';
    }

    public function getSites(): array {
        return $this->getEntries(WebflowSite::class);
    }

    public function getSiteByShortName(string $sn): ?WebflowSite {

        foreach ($this->getSites() as $site)
            if (isset($site->data['shortName']) && $site->data['shortName'] == $sn)
                return $site;

        return null;
    }

    public function createEntry($key, $value): ?AbstractWebflowApiField
    {
        return new WebflowSite($this->getClient(), $value);
    }
}

?>
