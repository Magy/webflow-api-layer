<?php

namespace App\Http\WebflowApi;

use App\Http\WebflowApiClient;

abstract class AbstractWebflowApiClient {

    private $client;

    public function __construct(WebflowApiClient $wfapi) {
        $this->client = $wfapi;
    }

    protected function getClient(): WebflowApiClient {
        return $this->client;
    }

    public function getLastResponseHeaders(): ?array {
        return $this->client->lastResponse;
    }

}

