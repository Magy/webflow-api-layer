<?php

namespace App\Http\WebflowApi;

class WebflowApiCollections extends AbstractWebflowApiCollection implements \IteratorAggregate{

    protected function getLoadScope(): string {
        return sprintf('sites/%s/collections', $this->parent->data['_id']);
    }

    public function createEntry($key, $value): ?AbstractWebflowApiField {
       return new WebflowApiCollection($this->getClient(), $value); 
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->getData());
    }

}
