<?php

namespace App\Http\WebflowApi;

interface WebflowPaginationInterface {
    public function getCount(): int;
    public function getLimit(): int;
    public function getOffset(): int;
    public function getTotal(): int;
    public function next();
}

class WebflowApiCollectionItems extends AbstractWebflowApiCollection implements WebflowPaginationInterface, \IteratorAggregate {

    private $count = 0;
    private $limit = 0;
    private $offset = 0;
    private $total = 0;

    protected function getLoadScope(): string
    {
        return sprintf("collections/%s/items", $this->parent->id['_id']);
    }

    public function createEntry($key, $value): ?AbstractWebflowApiField
    {
        return new WebflowApiCollectionItem($this->getClient(), $value);
    }

    public function getIterator(): \Traversable {
        return new \ArrayIterator($this->getData());
    }

    public function getCount(): int {
        return $this->count;
    }

    public function setCount(int $c): self {
        $this->count = $c;
        return $this;
    }

    public function getLimit(): int {
        return $this->limit;
    }

    public function setLimit(int $l): self {
        $this->limit = $l;
        return $this;
    }

    public function getOffset(): int {
        return $this->offset;
    }

    public function setOffset(int $o): self {
        $this->offset = $o;
        return $this;
    }

    public function getTotal(): int {
        return $this->total;
    }

    public function setTotal(int $i): self {
        $this->total = $i;
        return $this;
    }

    public function next() {
    
    }

}
