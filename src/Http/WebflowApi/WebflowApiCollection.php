<?php

namespace App\Http\WebflowApi;

class WebflowApiCollection extends AbstractWebflowApiField {

    protected function getLoadScope(): string
    {
        return sprintf('collections/%s', $this->data['_id']);
    }

    public function getItems(): ?WebflowApiCollectionItems {
        return $this->loadCollection(
            sprintf('collections/%s/items', $this->data['_id']),
            WebflowApiCollectionItems::class
        );
    }

    public function createItem(array $data): WebflowApiCollectionItem {
        $resp = $this->getClient()->post(
            $this->getLoadScope() . DIRECTORY_SEPARATOR . "items",
            $data
        );
        return new WebflowApiCollectionItem($this->getClient(), $resp->toArray());
    }
}
