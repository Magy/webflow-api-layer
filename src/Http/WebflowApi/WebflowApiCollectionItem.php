<?php

namespace App\Http\WebflowApi;

use App\Entity\AbstractWebflowCollectionItem;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WebflowApiCollectionItem extends AbstractWebflowApiField {
    
    protected function getLoadScope(): string
    {
        return sprintf('collections/%s/items/%s', $this->data['_cid'], $this->data['_id']);
    }

    public static function byId(AbstractWebflowApiClient $wfapi, string $id, array $options = []): AbstractWebflowApiField
    {
        $new = parent::byId($wfapi, $id);
        $new->data['_cid'] = $options['_cid'] ?? null;
        return $new;
    }

    public function load(bool $reload = false): self {
        $oldData = $this->data;

        parent::load($reload)->data = array_pop($this->data['items']);
        
        if (empty($this->data))
            throw new NotFoundHttpException(sprintf("Item with ID %s (cID %s) is not found", $oldData['_id'], $oldData['_cid']));

        return $this;
    }

}
