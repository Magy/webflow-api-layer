<?php

namespace App\Http;

use Psr\Log\LoggerAwareInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Adapter\TraceableAdapter;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class WebflowApiClient implements LoggerAwareInterface {

    private $client;
    private $cache;
    private $url;
    private $options = [];
    public $lastResponse;

    public function __construct(CacheInterface $cache, string $token, string $api_url, string $version = '1.0.0') {

        $this->url = $api_url;

        $this->options = [
            'auth_bearer' => $token,
            'headers' => [
                'accept-version' => $version,
            ],
        ];

        $this->client = HttpClient::createForBaseUri(
            sprintf('https://%s', $api_url), 
            $this->options
        );
        
        $this->cache = $cache;
    }


    public function scopeFromBase(string $scope): string {
        return sprintf('https://%s/%s', $this->url, ltrim($scope, '/'));
    }

    private function scopeId(string $scope_string): string {
        return "webflow_api_" . sha1($scope_string);
    }

    public function post(string $scope, array $data): ResponseInterface {
        $data = array_filter($data, function ($in) {
            if ($in === false)
                return true;

            return !empty($in);
        });

        $resp = $this->client->request('POST', $this->scopeFromBase(
            $scope
        ), ['json' => [
            "fields" => $data
        ]]);

        $this->lastResponse = $resp->getHeaders();

        $this->logger->debug(__METHOD__, [
            $resp->getStatusCode(), $resp->toArray(), $data
        ]);

        return $resp;
    }

    public function get(string $scope, int $ttl = 1800): ResponseInterface {

        $this->logger->debug(__METHOD__, ['ttl' => $ttl, 'scope' => $scope]);
        $id = $this->scopeId($scope);

        if ($ttl <= 0) {

            $resp = $this->client->request('GET',
                $this->scopeFromBase($scope),
            );

            $this->lastResponse = $resp->getHeaders();

            if ($resp->getStatusCode() >= 300)
                return $resp;

            $this->cache->get($id, function(ItemInterface $item) use ($scope, $ttl, $resp) {
                $item->set([
                    'header' => $resp->getHeaders(),
                    'content' => $resp->getContent(),
                ]);
                $this->cache->save($item);
            });

            $this->cache->commit();

            return $resp;
        }

        $item = $this->cache->get($id, function (ItemInterface $item) use ($scope, $ttl) {
            $item->expiresAfter($ttl);

            $response = $this->client->request('GET',
                $this->scopeFromBase($scope),
            );

            $this->lastResponse = $response->getHeaders();

            if ($response->getStatusCode() >= 300)
                return ['header' => $response->getHeaders(), "content" => $response->getContent()];

            $item->set([
                'header' => $response->getHeaders(),
                'content' => $response->getContent(),
            ]);

            $this->cache->save($item);

            return $item->get();
        });

        $this->cache->commit();

        $this->logger->debug(__METHOD__, [$scope, array_keys($item)]);

        if (!$item)
            return $this->get($scope, -1);

        $this->lastResponse = $item['header'];
        return MockResponse::fromRequest('GET', $scope, $item['header'], new MockResponse($item['content']));
    }

    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}

?>
