<?php

namespace App\Serializer;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 */
class ItemSerializedName {

    private $origName;

    public function __construct(array $data) {
        $this->origName = $data['value'] ?? null;

        if (empty($this->origName))
            throw new \InvalidArgumentException(sprintf("Value must me non-empty for %s", static::class));
    }

    public function getFieldName(): string {
        return $this->origName;
    }
}
