<?php

namespace App\Command;

use App\Entity\WebflowCollectionDates;
use App\Entity\WebflowCollectionEvent;
use App\Http\WebflowApi\WebflowApiCollection;
use App\Http\WebflowApi\WebflowApiCollectionItem;
use App\Http\WebflowApi\WebflowSites;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExcausedException extends \Exception {}

class WfAddDatesCommand extends Command
{
    protected static $defaultName = 'wf:add-dates';
    public $site;

    public function __construct(WebflowSites $webflowSites) {
        $this->site = $webflowSites->getSiteByShortName('tromsotid');
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $fn = getcwd() . "/var/dates";

        if (!file_exists($fn)) {
            $io->success("File do not exist, but that all ok! :)");
            return static::SUCCESS;
        }

        if (filesize($fn) <= 0) {
            return static::SUCCESS;
        }

        $data = null;

        try {
            $dateApi = WebflowApiCollection::byId($this->site, WebflowCollectionDates::cid())->load(true);
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            return static::FAILURE;
        }

        $fh = fopen($fn, "r+");

        fseek($fh, 0, SEEK_SET);
        flock($fh, LOCK_EX);

        if (filesize($fn) <= 0) {
            flock($fh, LOCK_UN);
            fclose($fh);
            return static::SUCCESS;
        }

        $fc = fread($fh, filesize($fn));

        $entries = array_filter(array_map(function (string $line) {
           return json_decode($line, true); 
        }, explode("\n", $fc)), function ($json) {
            return !is_null($json);
        });

        foreach ($entries as $lineno => $line) {

            try {
                $event = WebflowApiCollectionItem::byId($this->site,
                    $line['id'], [
                        '_cid' => WebflowCollectionEvent::cid()
                    ]
                )->load(true);
            } catch (NotFoundHttpException $e) {
                $io->caution($e->getMessage());
                unset($entries[$lineno]);
                continue;
            }

            try {
                foreach ($line['entries'] as $key => $entry) {
                    $limit = ($dateApi->getLastResponseHeaders()['x-ratelimit-limit']?? [60])[0];
                    $remaining = ($dateApi->getLastResponseHeaders()['x-ratelimit-remaining'] ?? [60])[0];

                    $ratio = $remaining / $limit;

                    if ($ratio < 0.3)
                        throw new ExcausedException("Ratio met.");

                    $item = $dateApi->createItem([
                        'name' => $entry['title'],
                        'dato' => $entry['date'],
                        'varighet' => $entry['duration'],
                        'arrangement' => [$line['id']],
                        '_draft' => true,
                        '_archived' => false
                    ]);
                    unset($line['entries'][$key]);
                }
            } catch (\Exception $e) {
                $io->caution($e->getMessage());
            } finally {
                $entries[$lineno] = $line;
            }
        }

        foreach ($entries as $key => $entry)
            if (sizeof($entry['entries']) == 0)
                unset($entries[$key]);

        fseek($fh, 0, SEEK_SET);
        ftruncate($fh, 0);

        if (sizeof($entries) != 0) {
            fwrite($fh, implode("\n", array_map(function ($json) {
                return json_encode($json);
            }, $entries)));
        }

        flock($fh, LOCK_UN);
        fclose($fh);

        return static::SUCCESS;
    }

}
