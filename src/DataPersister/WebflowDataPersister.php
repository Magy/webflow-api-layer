<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\WebflowCollectionEvent;
use App\Http\WebflowApi\WebflowApiCollection;
use App\Http\WebflowApi\WebflowSites;
use App\Serializer\ItemSerializer;
use Doctrine\Common\Annotations\Reader;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Security\Core\Exception\InsufficientAuthenticationException;

final class WebflowDataPersister implements ContextAwareDataPersisterInterface{

    private $logger;
    private $site;

    private $reader;

    public function __construct(LoggerInterface $loggerInterface, WebflowSites $webflowSites, Reader $reader)
    {
        $this->logger = $loggerInterface;
        $this->logger->debug(__METHOD__, $webflowSites->getSites());
        $this->site = $webflowSites->getSiteByShortName('tromsotid');
        $this->reader = $reader;
    }

    public function supports($data, array $context = []): bool
    {
        $this->logger->debug(__METHOD__, [
            'data' => $data,
        ] + $context);

        return $data instanceof WebflowCollectionEvent;
        
    }

    public function persist($data, array $context = [])
    {
        $val = [
            'id' => $data->getId(),
            'cid' => $data->getCid(),
            'name' => $data->getTitle(),
            'place' => $data->getPlace(),
        ];

        $client = HttpClient::create();

        $resp = $client->request('POST', "https://www.google.com/recaptcha/api/siteverify", [
            'body' => [
                'secret' => '6LfRxgAVAAAAAJBFO492ap_OUfQz3kJotl2Xad_V',
                'response' => $data->getCaptcha(),
                'remoteip' => $_SERVER['REMOTE_ADDR'],
            ]
        ]);

        $content = $resp->toArray();

        if (!$content['success'])
            throw new InsufficientAuthenticationException(sprintf("Error-codes\nAuthentication: %s (captcha)", join(", ", $content['error-codes'])));

        $dates = array_map(function ($data) {
            $data['date'] = new \DateTime($data['date']);
            return $data;
        }, $data->getHappensOn());

        $itemSerializer = new ItemSerializer($this->reader, $data);
        
        $data->setHappensOn([]);

        $item = WebflowApiCollection::byId($this->site, $data->getCid())->createItem(
            $itemSerializer->restoreProperties()
        );

        $data = WebflowCollectionEvent::fromClient($item, $this->reader);

        $fh = fopen(getcwd() . '/../var/dates', "a+");
        flock($fh, LOCK_EX);

        $entries = [];

        foreach ($dates as $date) {
            $entries[] = [
                'date' => $date['date']->format(\DateTime::RFC3339_EXTENDED),
                'duration' => $date['duration'],
                'title' => sprintf("%s, %s -> %dh", $data->getTitle(), $date['date']->format('d-m-Y H:i'), $date['duration']),
            ];
        }

        fwrite($fh, json_encode([
            'id' => $data->getId(),
            'entries' => $entries,
        ]) . "\n");

        flock($fh, LOCK_UN);
        fclose($fh);

        return $data;
    }

    public function remove($data, array $context = [])
    {
        
    }
}
