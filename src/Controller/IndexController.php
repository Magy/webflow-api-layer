<?php

namespace App\Controller;

use App\Entity\ItemSerializedName;
use App\Entity\WebflowCollectionEvent;
use App\Http\WebflowApi\WebflowApiCollection;
use App\Http\WebflowApi\WebflowApiCollections;
use App\Http\WebflowApi\WebflowSites;
use App\Http\WebflowApi\WebflowSite;
use App\Http\WebflowApiClient;
use App\Serializer\ItemSerializer;
use Doctrine\Common\Annotations\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Annotation\SerializedName;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(WebflowSites $wfsites, WebflowApiClient $wfapi, Reader $reader)
    {
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    /**
     * @Route(
     *  "/proxy/{type}/{name}",
     * )
    public function proxyCategory(): Response {
        return null;
    }
     */
}
