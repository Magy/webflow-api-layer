<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\WebflowCollection;
use App\Entity\WebflowCollectionAudience;
use App\Entity\WebflowCollectionCategory;
use App\Entity\WebflowCollectionDates;
use App\Entity\WebflowCollectionEvent;
use App\Http\WebflowApi\WebflowApiCollection;
use App\Http\WebflowApi\WebflowSite;
use App\Http\WebflowApi\WebflowSites;
use App\Http\WebflowApiClient;
use App\Serializer\ItemSerializer;
use Doctrine\Common\Annotations\Reader;
use phpDocumentor\Reflection\Types\Resource_;
use Psr\Log\LoggerInterface;

final class WebflowDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface {

    private $site;
    private $apiClient;
    private $logger;
    private $reader;

    public function __construct(WebflowApiClient $webflowApiClient, LoggerInterface $l, Reader $reader) {
        $this->site = WebflowSite::byId(new WebflowSites($webflowApiClient), '5ebabfe546c816388d66c03a');
        $this->apiClient = $webflowApiClient;
        $this->logger= $l;
        $this->reader = $reader;
    }

    public function supports(string $resourceClass, ?string $operationName = null, array $context = []): bool
    {
        $this->logger->debug(__METHOD__, [$resourceClass, $operationName, $context]);
        return in_array($resourceClass, [
            WebflowCollection::class,
            WebflowCollectionAudience::class,
            WebflowCollectionCategory::class,
            WebflowCollectionDates::class,
            WebflowCollectionEvent::class,
        ]);
    }

    public function getCollection(string $resourceClass, ?string $operationName = null, array $context = []): \Generator
    {
        switch ($resourceClass) {
        case WebflowCollection::class:
            foreach($this->site->getCollections() as $col) {
                $class =  WebflowCollection::fromClient($col, $this->reader);
                yield $class;
            }
            break;
        case WebflowCollectionAudience::class:
        case WebflowCollectionCategory::class:
        case WebflowCollectionDates::class:
        case WebflowCollectionEvent::class:
            $col = WebflowApiCollection::byId($this->site, $resourceClass::cid())->load();

            foreach ($col->getItems() as $item) {
                 $class = $resourceClass::fromClient($item, $this->reader);
                 yield $class;
            }
        }

        return null;
    }
}
