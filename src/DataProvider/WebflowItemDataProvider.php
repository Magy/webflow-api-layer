<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\WebflowCollection;
use App\Entity\WebflowItem;
use App\Entity\WebflowItemCategory;
use App\Http\WebflowApi\WebflowSite;
use App\Http\WebflowApi\WebflowSites;
use App\Http\WebflowApiClient;
use Psr\Log\LoggerInterface;

final class WebflowItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface {

    private $site;
    private $apiClient;
    private $logger;

    public function __construct(WebflowApiClient $webflowApiClient, LoggerInterface $loggerInterface)
    {
        $this->site = WebflowSite::byId($webflowApiClient, '5ebabfe546c816388d66c03a');
        $this->logger = $loggerInterface;
    }

    public function supports(string $resourceClass, ?string $operationName = null, array $context = []): bool {
        $this->logger->debug(__METHOD__, [$resourceClass, $operationName, $context]);
        return in_array($resourceClass, [
            WebflowCollection::class,
            WebflowItem::class,
        ]);
    }

    public function getItem(string $resourceClass, $id, ?string $operationName = null, array $context = []): ?WebflowCollection {
        switch ($resourceClass) {
        case WebflowCollection::class:
            foreach ($this->site->getCollections() as $col)
                if ($col->data['_id'] == $id) {
                    return new WebflowCollection($col->load(true));
                }
            break;
        }
    
        return null;
    }

}
