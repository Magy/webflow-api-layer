import React, {useState} from "react";
import axios from "axios";
import DateFnsUtils from '@date-io/date-fns';
import {nb} from 'date-fns/locale'
import {DateTimePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import {makeStyles} from '@material-ui/core/styles';

import ReCAPTCHA from "react-google-recaptcha";

import {
    Button,
    ButtonGroup,
    Container,
    Checkbox,
    CssBaseline,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    FormControl,
    FormControlLabel,
    FormGroup,
    Grid,
    IconButton,
    Paper,
    TextField,
    Typography,
} from '@material-ui/core';

import {
    Alert,
    AlertTitle
} from '@material-ui/lab';

import {
    Add,
    Remove,
} from '@material-ui/icons'

const useStyles = makeStyles((theme) => ({
    fieldset: {
        //border: 'none',
        marginBottom: theme.spacing(3),
        paddingBottom: theme.spacing(3),
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
    }
}));

const ReCaptchaSubmitButton = (props) => {

    const recaptchaRef = React.createRef();
    const [response, setResponse] = useState(null);

    const onChange = (value) => {
        if (value) {
            const request = {
                organizer: props.inputs.organizer || null,
                organizationNumber: props.inputs.organization_number || null,
                contactPerson: props.inputs.contact_name || null,
                contactEmail: props.inputs.contact_email || null,
                contactPhone: props.inputs.contact_phone || null,
                websiteUrl: props.inputs.contact_homepage || null,
                place: props.inputs.event_location || null,
                capacity: props.inputs.event_capacity || null,
                happensOn: props.inputs.dates || [],
                title: props.inputs.event_name || null,
                image: props.inputs.event_photo || null,
                description: props.inputs.event_description || null,
                bookingUrl: props.inputs.event_booking || null,
                audiences: (props.inputs.audiences || []).map(i => i.id),
                categories: (props.inputs.categories || []).map(i => i.id),
                captcha: value,
            };

            axios.post("https://magy.giaever.online/tff/api/webflow_collection_events", request).then(resp => {
                setResponse({type: 'success', title: "Takk for ditt bidrag!", description: "Vi ser frem til din deltagelse."});
                props.clearHandler();
            }).catch(err => {
                if (err.response) {
                    setResponse({type: 'error', title: err.response.data['hydra:title'], description: err.response.data['hydra:description']});
                } else if (err.request)
                    setResponse({type: 'error', title: "En feil oppstod.", description: "Vennligst prøv igjen senere."});
                else
                    setResponse({type: 'error', title: "En feil oppstod,", description: err.message});
            });
        }
    }

    return (
        <React.Fragment>
            <center>
            <Dialog open={response!= null} onClose={() => {
                setResponse(null);
                recaptchaRef.current.reset();
            }}>
                <DialogContent>
                    <Alert severity={response ? response.type : "info"}>
                        <AlertTitle>{response ? response.title : "unknown title"}</AlertTitle>
                        {response ? (
                            response.type == "error" ? 
                                <ul>{response.description.split("\n").map((l, i) => <li key={i}>{l}</li>)}</ul>
                                :
                                response.description
                            )
                        : "this is the body"}
                    </Alert>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                        setResponse(null);
                        recaptchaRef.current.reset();
                        if (response.type != 'error')
                            window.location.reload();
                    }} color="secondary">
                        Ok!
                    </Button>
                </DialogActions>
            </Dialog>
                <ReCAPTCHA
                    onChange={onChange}
                    ref={recaptchaRef}
                    sitekey={"6LfRxgAVAAAAALlMOyD5h_a6gvWulxGqCC5gKJvd"}
                    size={"invisible"}
                />
            </center>
            <Button 
                color={"primary"}
                variant={"contained"}
                fullWidth
                type={"submit"}
                onClick={(e) => {
                    e.preventDefault();
                    recaptchaRef.current.execute();
                }}
            >Send skjema</Button>
        </React.Fragment>
    );

}

const RegForm = (props) => {
    const classes = useStyles();

    const [inputs, updateInputs] = useState({
        dates: [{date: new Date(), duration: 0.5}],
    });

    const inputHandler = (event) => {
        const target = event.target
        updateInputs((prevState) => ({...prevState, [target.id]: target.value}));
    };

    const inputCategoryHandler = (type, event, category) => {
        const keys = Object.keys(inputs).filter(idx => idx == type);
        let arr = keys.length == 0 ? [] : inputs[keys[0]];

        if (event.target.checked)
            arr.push(category);
        else 
            arr = arr.filter(item => item.id != category.id);

        updateInputs((prevState) => ({...prevState, [type]: arr}));
    }

    const dateAddHandler = (idx) => {
        inputs.dates.splice(idx, 0, {...inputs.dates[idx]});
        updateInputs((prevState) => ({...prevState, dates: inputs.dates}));
    }

    const dateRemoveHandler = (idx) => {
        if (inputs.dates.length == 1)
            return;

        if (!inputs.dates[idx])
            return;

        updateInputs(prevState => ({...prevState, dates: [...inputs.dates.slice(0, idx), ...inputs.dates.slice(idx+1)]}));
    }

    const dateSaveHandler = (idx, e) => {
        if (!inputs.dates[idx])
            return;
        inputs.dates[idx].date = e;
        updateInputs((prevState) => ({...prevState, dates: inputs.dates}));
    }

    const dateDurationSaveHandler = (idx, e) => {
        if (!inputs.dates[idx])
            return;
        
        const target = e.target;
        inputs.dates[idx].duration = (target.value >= 0 ? target.value * 1.0 : 0.5).toFixed(2);

        updateInputs((prevState) => ({...prevState, dates: inputs.dates}));
    }

    let formRef = React.createRef();

    const clearInputs = () => {
        formRef.current.reset();
        updateInputs((prevState) => ({
            dates: [{date: new Date(), duration: 0.5}]
        }));
    }

    return (
        <Container component="main" maxWidth="md">
            <CssBaseline />
            <Paper className={classes.paper}>
                <form className={classes.form} ref={formRef}>
                    <FormControl component={"fieldset"} className={classes.fieldset}>
                        <Typography component={"legend"} variant={"h5"}>
                            Om arrangøren
                        </Typography>
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={7} md={8}>
                                <TextField 
                                    id={"organizer"}
                                    label={"Navn"}
                                    onChange={inputHandler}
                                    required
                                    fullWidth
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12} sm={5} md={4}>
                                <TextField 
                                    id={"organization_number"}
                                    label={"Org.nummer"}
                                    onChange={inputHandler}
                                    required
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField 
                                    id={"contact_name"}
                                    label={"Kontakt person"}
                                    onChange={inputHandler}
                                    required
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id={"contact_email"}
                                    label={"Epostadresse"}
                                    onChange={inputHandler}
                                    type={"email"}
                                    required
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id={"contact_phone"}
                                    label={"Telefonnummer"}
                                    onChange={inputHandler}
                                    type={"tel"}
                                    required
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id={"contact_homepage"}
                                    label={"Hjemmesideadresse (husk «http://»)"}
                                    placeholder={"https:// ...."}
                                    onChange={inputHandler}
                                    required
                                    fullWidth
                                />
                            </Grid>
                        </Grid>
                    </FormControl>
                    <FormControl component={"fieldset"} className={classes.fieldset}>
                        <Typography component={"legend"} variant={"h5"}>
                            Om arrangementet
                        </Typography>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <TextField
                                    id={"event_name"}
                                    label={"Tittel på arrangement"}
                                    onChange={inputHandler}
                                    required
                                    fullWidth
                                />
                            </Grid>
                            {inputs.dates.map((date, idx) => (
                                <React.Fragment key={idx}>
                                    <Grid item xs={6}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={nb}>
                                            <DateTimePicker 
                                                fullWidth 
                                                format={"do MMMM y HH:mm (h:mm a)"} 
                                                ampm={false} 
                                                value={date.date} 
                                                onChange={e => dateSaveHandler(idx, e)} 
                                                margin={"normal"} 
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <TextField
                                            id={"event_duration"}
                                            label={`Varighet (${date.duration} t = ${(date.duration * 60.0).toFixed(0)} min)`}
                                            onChange={e => dateDurationSaveHandler(idx, e)}
                                            inputProps={{step: 0.25}}
                                            type={"number"}
                                            placeholder={"antall timer"}
                                            value={date.duration ? date.duration : 0.5}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item xs={1}>
                                        <IconButton onClick={() => dateAddHandler(idx)}><Add /></IconButton>
                                    </Grid>
                                    <Grid item xs={1}>
                                        <IconButton  onClick={() => dateRemoveHandler(idx)}><Remove /></IconButton>
                                    </Grid>
                                </React.Fragment>
                            ))}
                            <Grid item xs={12} sm={8}>
                                <TextField
                                    id={'event_location'}
                                    label={"Sted"}
                                    onChange={inputHandler}
                                    placeholder={"Hvor skjer arrangementet?"}
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <TextField
                                    id={"event_capacity"}
                                    label={"Kapasitet"}
                                    placeholder={"For eks. antall personer"}
                                    onChange={inputHandler}
                                    required
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id={"event_description"}
                                    label={"Beskrivelse av arrangement"}
                                    onChange={inputHandler}
                                    placeholder={"Forklar detaljert om ditt arrangement - dette skal besøkende lese!"}
                                    multiline
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="event_photo"
                                    label={"Photo-link til arrangement (husk «http://»)"}
                                    onChange={inputHandler}
                                    placeholder={"Kan for eks. være en bilde-fil eller mappe med bilder i Dropbox."}
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    id="event_booking"
                                    label={"Link til booking (husk «http://»)"}
                                    onChange={inputHandler}
                                    placeholder={"https:// ..."}
                                    fullWidth
                                    required
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <CategorySelection 
                                    cid={"/tff/api/webflow_collection_categories"} 
                                    category="Kategorier"
                                    name="categories"
                                    selected={inputs.categories || []}
                                    inputCategoryHandler={inputCategoryHandler}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <CategorySelection 
                                    cid={"/tff/api/webflow_collection_audiences"} 
                                    category="Målgrupper"
                                    name="audiences"
                                    selected={inputs.audiences || []}
                                    inputCategoryHandler={inputCategoryHandler}
                                />
                            </Grid>
                        </Grid>
                    </FormControl>
                    <Grid container justify={"center"}>
                        <Grid item xs={8}>
                            <ReCaptchaSubmitButton text={"Send skjema"} 
                                inputs={inputs} 
                                clearHandler={clearInputs}
                            />
                        </Grid>
                    </Grid>
                </form>
            </Paper>
        </Container>
    )
}

const CategorySelection = (props) => {
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [availableItems, setAvailableItems] = useState([]);

    React.useEffect(() => {
        axios.get("https://magy.giaever.online" + props.cid)
            .then((response) => {
                const data = response.data['hydra:member'];
                setAvailableItems(response.data['hydra:member']);
            })
            .catch((error) => {
                setError(error);
            })
            .then(() => {
                setIsLoading(false);
            });
    }, []);

    return (
        <React.Fragment>
            <DialogCancelOk
                buttonDesc={isLoading ? `Laster ${props.category}` : `Velg ${props.category}`}
                buttonDisabled={isLoading}
                dialogTitle={`Velg ${props.category}`}
            >
                {isLoading ? 
                    <p>
                        Laster...
                    </p>
                    :
                    <FormGroup>
                        {availableItems.map(item => <FormControlLabel 
                            key={item.id}
                            control={
                                <Checkbox 
                                    checked={props.selected.filter(sitem => sitem.id == item.id).length > 0} 
                                    value={item.id}
                                    onChange={() => props.inputCategoryHandler(props.name, event, item)}
                                />
                            }
                            label={item.name}
                        />
                        )}
                    </FormGroup>
                }
            </DialogCancelOk>
            <ul>{props.selected.map(item => <li key={item.id}>{item.name}</li>)}</ul>
        </React.Fragment>
    )
}

const DialogCancelOk = (props) => {
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClickClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button 
                fullWidth
                disabled={props.buttonDisabled}
                color={"secondary"}
                variant={props.buttonDisabled ? "outlined" : "contained"}
                onClick={handleClickOpen}>
                    {props.buttonDesc}
            </Button>
            <Dialog 
                disableBackdropClick
                disableEscapeKeyDown
                open={open}
                onClose={handleClickClose}
            >
                <DialogTitle>{props.dialogTitle}</DialogTitle>
                <DialogContent>
                    {props.children}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClickClose} color={"primary"}>
                        Ferdig
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default RegForm;
